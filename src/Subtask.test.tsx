import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';

import Subtask from './Subtask';

describe('<Subtask />', () => {
  const props = {
    title: 'Do laundry',
    isCompleted: false,
    toggleSubtask: jest.fn(),
  };

  it('renders the correct title', () => {
    render(<Subtask {...props} />);

    expect(screen.getByText('Do laundry')).toBeInTheDocument();
  });

  it('adds the "completed" class when isCompleted is true', () => {
    render(<Subtask {...props} isCompleted={true} />);

    expect(screen.getByText('Do laundry')).toHaveClass('completed');
  });

  it('calls toggleSubtask when clicked', () => {
    render(<Subtask {...props} />);

    fireEvent.click(screen.getByText('Do laundry'));

    expect(props.toggleSubtask).toHaveBeenCalledTimes(1);
  });
});
