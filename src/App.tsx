import React, { useState } from 'react';

import './App.css';
import Task from './Task';
import TaskItem from './TaskItem';

const data: TaskItem[] = [{
  title: 'Clean Bedroom',
  isVisible: true,
  isCompleted: false,
  subtasks: [{
    title: 'Do laundry',
    isCompleted: false,
  }, {
    title: 'Organize desk',
    isCompleted: false,
  }, {
    title: 'Wipe floors',
    isCompleted: false,
  }],
}, {
  title: 'Study',
  isVisible: true,
  isCompleted: false,
  subtasks: [{
    title: 'Review chemistry',
    isCompleted: false,
  }, {
    title: 'Do a React coding challenge',
    isCompleted: false,
  }],
}, {
  title: 'Build Website',
  isVisible: true,
  isCompleted: false,
  subtasks: [{
    title: 'Choose tech stack',
    isCompleted: false,
  }, {
    title: 'Design pages',
    isCompleted: false,
  }, {
    title: 'Develop',
    isCompleted: false,
  }, {
    title: 'Publish',
    isCompleted: false,
  }],
}];

const App = () => {
  const [tasks, setTasks] = useState(data);

  const toggleTask = (index: number, subtaskIndex: number) => {
    const { subtasks } = tasks[index];

    const newSubtasks = [...subtasks.slice(0, subtaskIndex), {
      ...subtasks[subtaskIndex],
      isCompleted: !subtasks[subtaskIndex].isCompleted,
    }, ...subtasks.slice(subtaskIndex + 1)];
    const isTaskCompleted = newSubtasks.every(({ isCompleted }) => isCompleted);
    const newTask = {
      ...tasks[index],
      isCompleted: isTaskCompleted,
      subtasks: newSubtasks,
    };

    const newTasks = [...tasks.slice(0, index), newTask, ...tasks.slice(index + 1)];

    setTasks(newTasks);
  }

  const clearCompletedTasks = () => {
    const newTasks: TaskItem[] = [];

    tasks.forEach(task => {
      if (task.isCompleted) {
        newTasks.push({
          ...task,
          isVisible: false,
        });
      } else {
        newTasks.push(task);
      }
    })

    setTasks(newTasks);
  }

  return (
    <div className="app">
      <button className="clear-button" onClick={clearCompletedTasks}>Clear completed tasks</button>

      {tasks.map(({ title, isVisible, isCompleted, subtasks }, index) => (
        <Task key={title}
              title={title}
              isVisible={isVisible}
              isCompleted={isCompleted}
              subtasks={subtasks}
              toggleTask={subtaskIndex => toggleTask(index, subtaskIndex)}
        />
      ))}
    </div>
  );
};

export default App;
