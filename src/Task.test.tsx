import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import Task from './Task';

describe('Task component', () => {
  const props = {
    title: 'Example Task',
    isVisible: true,
    isCompleted: false,
    subtasks: [
      { title: 'Subtask 1', isCompleted: false },
      { title: 'Subtask 2', isCompleted: true },
    ],
    toggleTask: jest.fn(),
  };

  it('renders the task title', () => {
    render(<Task {...props} />);

    expect(screen.getByText('Example Task')).toBeInTheDocument();
  });

  it('renders the subtasks', () => {
    render(<Task {...props} />);

    expect(screen.getByText('Subtask 1')).toBeInTheDocument();
    expect(screen.getByText('Subtask 2')).toBeInTheDocument();
  });

  it('calls toggleTask when a subtask is clicked', () => {
    render(<Task {...props} />);

    fireEvent.click(screen.getByText('Subtask 1'));

    expect(props.toggleTask).toHaveBeenCalledWith(0);
  });

  it('applies the "hidden" class when isVisible is false', () => {
    render(<Task {...props} isVisible={false} />);

    expect(screen.getByTestId('task')).toHaveClass('hidden');
  });

  it('applies the "completed" class when isCompleted is true', () => {
    render(<Task {...props} isCompleted={true} />);

    expect(screen.getByTestId('task')).toHaveClass('completed');
  });
});
