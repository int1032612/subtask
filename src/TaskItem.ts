import SubtaskItem from './SubtaskItem';

interface TaskItem {
  title: string;
  isVisible: boolean;
  isCompleted: boolean;
  subtasks: SubtaskItem[];
}

export default TaskItem;
