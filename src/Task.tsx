import React from 'react';

import Subtask from './Subtask';
import TaskItem from './TaskItem';

interface Props extends TaskItem {
  toggleTask: (subtaskIndex: number) => void;
}

const Task = ({ title, isVisible, isCompleted, subtasks, toggleTask }: Props) => {
  let className = 'task';
  className += !isVisible ? ' hidden' : '';
  className += isCompleted ? ' completed' : '';

  return (
    <div className={className} data-testid="task">
      <div className="title">{title}</div>

      {subtasks.map((subtask, index) =>
        <Subtask key={subtask.title}
                 title={subtask.title}
                 isCompleted={subtask.isCompleted}
                 toggleSubtask={() => {
                   toggleTask(index)
                 }}
        />
      )}
    </div>
  );
};

export default Task;
