interface SubtaskItem {
  title: string;
  isCompleted: boolean;
}

export default SubtaskItem;
