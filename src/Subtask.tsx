import React from 'react';

import SubtaskItem from './SubtaskItem';

interface Props extends SubtaskItem {
  toggleSubtask: () => void;
}

const Subtask = ({ title, isCompleted, toggleSubtask }: Props) => {
  return <div className={'subtask' + (isCompleted ? ' completed' : '')} onClick={() => toggleSubtask()}>{title}</div>;
};

export default Subtask;
