import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import App from './App';

describe('<App />', () => {
  it('renders without crashing', () => {
    render(<App />);
  });

  it('renders the correct number of tasks', () => {
    render(<App />);

    expect(screen.getAllByTestId('task')).toHaveLength(3);
  });

  it('toggles a subtask when clicked', () => {
    render(<App />);

    const subtask = screen.getAllByText('Do laundry')[0];
    fireEvent.click(subtask);

    expect(subtask).toHaveClass('completed');
  });
});
